gnome-shell-extension-impatience (0.5.2-2) unstable; urgency=medium

  * Add patch to mark compatible with GNOME Shell 48 (Closes: #1096051)
  * Bump maximum GNOME Shell to 48

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 23 Feb 2025 16:04:47 -0500

gnome-shell-extension-impatience (0.5.2-1) unstable; urgency=medium

  * New upstream release
  * Update gnome-shell dependency to include version 47
  * Remove patch for gnome-shell version 47 support
    (no longer needed)

 -- Jonathan Carter <jcc@debian.org>  Mon, 07 Oct 2024 12:23:32 +0200

gnome-shell-extension-impatience (0.5.1-2) unstable; urgency=medium

  * Team upload
  * Add patch to mark compatible with GNOME Shell 47 (Closes: #1079252)
  * Bump maximum GNOME Shell to 47

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 12 Sep 2024 19:08:48 -0400

gnome-shell-extension-impatience (0.5.1-1) unstable; urgency=medium

  * Team upload
  * Release to unstable (Closes: #1052104

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 15 Jul 2024 08:02:25 -0400

gnome-shell-extension-impatience (0.5.1-1~exp1) experimental; urgency=medium

  * New upstream release
  * Update standards version to 4.7.0
  * Update gnome-shell dependency to include version 46

 -- Jonathan Carter <jcc@debian.org>  Wed, 24 Apr 2024 10:32:19 +0200

gnome-shell-extension-impatience (0.5.0-1) experimental; urgency=medium

  * New upstream release
  * Update gnome-shell dependency for version 45
    (Closes: #1052104, not yet in unstable, uploading to experimental)
  * Remove debian/patches (no longer needed)

 -- Jonathan Carter <jcc@debian.org>  Wed, 15 Nov 2023 19:26:48 +0200

gnome-shell-extension-impatience (0.4.8-3) unstable; urgency=medium

  * Declare support for gnome-shell 44 in metadata.json (Closes: #1041575)

 -- Jonathan Carter <jcc@debian.org>  Tue, 29 Aug 2023 13:00:45 +0200

gnome-shell-extension-impatience (0.4.8-2) unstable; urgency=medium

  * Team upload
  * Add patch to mark compatible with GNOME Shell 43
  * debian/control: Bump maximum gnome-shell to 43 (Closes: #1018271)

 -- Jesús Soto <jesus.soto@canonical.com>  Thu, 08 Sep 2022 16:07:06 -0400

gnome-shell-extension-impatience (0.4.8-1) unstable; urgency=medium

  * New upstream release (Closes: #1008551)
  * Depend on gnome-shell (>= 40), gnome-shell (<< 43)

 -- Jonathan Carter <jcc@debian.org>  Wed, 20 Apr 2022 19:17:10 +0200

gnome-shell-extension-impatience (0.4.6+20220329-a2e06be-2) unstable; urgency=medium

  * Declare minimum gnome-shell version requirement of version 40

 -- Jonathan Carter <jcc@debian.org>  Wed, 30 Mar 2022 12:29:08 +0200

gnome-shell-extension-impatience (0.4.6+20220329-a2e06be-1) unstable; urgency=medium

  * New upstream snapshot (Closes: #1008551)

 -- Jonathan Carter <jcc@debian.org>  Wed, 30 Mar 2022 12:21:50 +0200

gnome-shell-extension-impatience (0.4.6-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Tue, 26 Oct 2021 10:17:04 +0200

gnome-shell-extension-impatience (0.4.5+git20210930-db933e8-1) unstable; urgency=medium

  * New upstream snapshot (Closes: #996064)
  * Fix incorrect standards version (Set to 4.6.0)

 -- Jonathan Carter <jcc@debian.org>  Wed, 20 Oct 2021 14:52:55 +0200

gnome-shell-extension-impatience (0.4.5+git20210412-e8e132f-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Jonathan Carter ]
  * New upstream snapshot (Closes: #993190)
  * Update standards version to 4.6.1
  * Upgrade to debhelper-compat to level 13
  * Declare Rules-Requires-Root: no
  * Update copyright years
  * Remove patch for GNOME 3.34 that's no longer needed

 -- Jonathan Carter <jcc@debian.org>  Thu, 02 Sep 2021 18:06:05 +0200

gnome-shell-extension-impatience (0.4.5-4) unstable; urgency=medium

  * Move Vcs to salsa.debian.org
  * Update standards version to 4.5.0
  * Upgrade to debhelper-compat (= 12)
  * Update copyright years
  * Use patch from Ubuntu to fix Gnome 3.34 compatibility
    - Breaks backwards compatibility, set minimum gnome-shell version to 3.34

 -- Jonathan Carter <jcc@debian.org>  Tue, 25 Feb 2020 11:00:17 +0200

gnome-shell-extension-impatience (0.4.5-3) unstable; urgency=medium

  * Update compat to level 10
  * Update standards version to 4.1.1
  * Update maintainer e-mail address

 -- Jonathan Carter <jcc@debian.org>  Wed, 18 Oct 2017 09:04:45 +0200

gnome-shell-extension-impatience (0.4.5-2) unstable; urgency=medium

  * Upload to unstable (no changes)

 -- Jonathan Carter <jcarter@linux.com>  Wed, 05 Jul 2017 14:52:42 +0200

gnome-shell-extension-impatience (0.4.5-1) experimental; urgency=medium

  * New upstream release
  * Update watch file to format version 4

 -- Jonathan Carter <jcarter@linux.com>  Sat, 27 May 2017 14:53:53 +0200

gnome-shell-extension-impatience (0.4.3-1) unstable; urgency=medium

  * Initial release (Closes: #842014)

 -- Jonathan Carter <jcarter@linux.com>  Sun, 30 Oct 2016 07:44:35 +0200
